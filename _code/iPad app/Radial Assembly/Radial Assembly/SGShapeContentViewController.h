//
//  SGShapeContentViewController.h
//  Radial Assembly
//
//  Created by Shubham Goel on 5/29/14.
//  Copyright (c) 2014 Shubham Goel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGShapeContentViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property NSUInteger pageIndex;
@property NSString *titleText;
@property (strong, nonatomic) NSArray *points;
@end
