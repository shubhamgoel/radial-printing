//
//  SGShapeView.m
//  Radial Assembly
//
//  Created by Shubham Goel on 5/28/14.
//  Copyright (c) 2014 Shubham Goel. All rights reserved.
//

#import "SGShapeView.h"
#import <math.h>
#define DEGREES(radians) (radians * 180 / M_PI)

@interface SGShapeView ()
- (NSMutableArray *)processPoints:(NSArray *)points;
@property (nonatomic, retain) NSMutableArray *points;
@end

@implementation SGShapeView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    if ([self.points count] > 0)
    {
        /* Drawing code */ 
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextBeginPath(context);
        
        NSUInteger index = 0;
        
//        NSMutableArray *modifiedPoints = [self processPoints:self.points];
//        self.points = modifiedPoints;
        
        for (NSArray *point in self.points)
        {
            ///TODO: remove scale and shift math
            float scale = 1.0;
            float x_shift = 500;
            float y_shift = 700;
            CGFloat x = scale*[point[0] floatValue]*-1 + x_shift;
            CGFloat y = scale*[point[1] floatValue]*-1 + y_shift;
            
            if(index == 0)
            {
                CGContextMoveToPoint(context, x, y);
            }else
            {
                CGContextAddLineToPoint(context, x, y);
            }
            index++;
        }
        
        CGContextClosePath(context);
        CGContextSetLineWidth(context, 2.0);
        
        UIColor *orange = [UIColor colorWithRed:229.0/255.0 green:137.0/255.0 blue:27.0/255.0 alpha:1.0];
        UIColor *darkGray = [UIColor colorWithRed:55.0/255.0 green:55.0/255.0 blue:51.0/255.0 alpha:1.0];
        [darkGray setFill];
        [orange setStroke];
        
        CGContextDrawPath(context, kCGPathFillStroke);
    }
}


- (NSMutableArray *)processPoints:(NSArray *)points
{
    /*
     triangle
     <------p12------>
     +1-------------+2
     |         a   -
     |         -
     |     -
     | -
     +3
     
     */
    // Detect notch
    int numberOfPoints = [points count];
    for (int i = 0; i < numberOfPoints; i++)
    {
        float x1,y1,x2,y2,x3,y3;
        int point2Index = i+1;
        int point3Index = i+2;
        
        x1 = [points[i][0] floatValue];
        y1 = [points[i][1] floatValue];
        
        // Distance test
        ///TODO: should come from config
        if (x1 > 1)
            continue;
        
        if (i == numberOfPoints - 2)
        {
            point2Index = i+1;
            point3Index = 0;

        }else if (i == numberOfPoints - 1)
        {
            point2Index = 0;
            point3Index = 1;
        }
        
        x2 = [points[point2Index][0] floatValue];
        y2 = [points[point2Index][1] floatValue];
        
        x3 = [points[point3Index][0] floatValue];
        y3 = [points[point3Index][1] floatValue];

        
        float p12sq = powf(x1-x2, 2) + powf(y1-y2, 2);
        float p23sq = powf(x3-x2, 2) + powf(y3-y2, 2);
        float p13sq = powf(x1-x3, 2) + powf(y1-y3, 2);
        
        // length test
        if ((p12sq > 625) || (p12sq < 9) || (p23sq < 9) || (p13sq < 1))
            continue;
     
        /*
         angle 123 = cons-1(p12sq+p)
         */
        
        float angle123 = DEGREES(acosf((p12sq+p23sq-p13sq)/(2*sqrtf(p12sq*p23sq))));
        
        // angle test
        if (angle123 > 15)
            continue;
        
//        NSLog(@"Notch detected");
//        NSLog(@"P1: %f, %f", x1, y1);
//        NSLog(@"P2: %f, %f", x2, y2);
//        NSLog(@"P3: %f, %f", x3, y3);
//        NSLog(@"p12sq %f; p23sq %f",p12sq, p23sq);
        
        /* remove old notch */
        NSMutableArray *modifiedShape = [points mutableCopy];
        NSArray *p1 = modifiedShape[i];
        NSArray *p2 = modifiedShape[point2Index];
        NSArray *p3 = modifiedShape[point3Index];
        
        [modifiedShape removeObject:p1];
        [modifiedShape removeObject:p2];
        [modifiedShape removeObject:p3];
        
        /* read new notch */
        NSString *notchFilePath = [[NSBundle mainBundle] pathForResource:@"notch" ofType:@"json"];
        NSError *notchFileReadError;
        NSString *notchFileContents = [NSString stringWithContentsOfFile:notchFilePath encoding:NSUTF8StringEncoding error:&notchFileReadError];
        
        NSData *notchJsonData = [notchFileContents dataUsingEncoding:NSUTF8StringEncoding];
        NSError *notchJsonReadingError;
        NSMutableArray *notch = [NSJSONSerialization JSONObjectWithData:notchJsonData options:NSJSONReadingMutableContainers error:&notchJsonReadingError];
        
        /* move the new notch */
        for (int j = [notch count]-1; j > -1; j--)
        {
            NSNumber *notchX = [NSNumber numberWithFloat:[notch[j][0] floatValue]+x1];
            NSNumber *notchY = [NSNumber numberWithFloat:[notch[j][1] floatValue]+y1];
            
            [modifiedShape insertObject:@[notchX, notchY] atIndex:i];
        }
        
        return modifiedShape;
        
    }
    
    return [points mutableCopy];
}


- (void)forShapeSetPoints:(NSMutableArray *)points
{
    self.points = [points mutableCopy];
}
@end
