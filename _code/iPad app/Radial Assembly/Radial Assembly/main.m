//
//  main.m
//  Radial Assembly
//
//  Created by Shubham Goel on 5/28/14.
//  Copyright (c) 2014 Shubham Goel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SGAppDelegate class]));
    }
}
