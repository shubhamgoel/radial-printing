//
//  SGShapeView.h
//  Radial Assembly
//
//  Created by Shubham Goel on 5/28/14.
//  Copyright (c) 2014 Shubham Goel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGShapeView : UIView
- (void)forShapeSetPoints:(NSArray *)points;
@end
