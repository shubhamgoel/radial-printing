//
//  SGShapeContentViewController.m
//  Radial Assembly
//
//  Created by Shubham Goel on 5/29/14.
//  Copyright (c) 2014 Shubham Goel. All rights reserved.
//

#import "SGShapeContentViewController.h"
#import "SGShapeView.h"

@interface SGShapeContentViewController ()

@end

@implementation SGShapeContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIColor *greyColor = [UIColor colorWithRed:(184.0/255.0) green:(193.0/255.0) blue:(202.0/255.0) alpha:1.0];
    self.view.backgroundColor = greyColor;
    
    self.titleLabel.text =  self.titleText;
    
    SGShapeView *shapeView = [[SGShapeView alloc] initWithFrame:CGRectMake(10, 20, 600, 800)];

    shapeView.backgroundColor = greyColor;
    [shapeView forShapeSetPoints:self.points];
    [self.view addSubview:shapeView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
