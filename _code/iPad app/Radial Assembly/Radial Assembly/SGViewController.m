//
//  SGViewController.m
//  Radial Assembly
//
//  Created by Shubham Goel on 5/28/14.
//  Copyright (c) 2014 Shubham Goel. All rights reserved.
//

#import "SGViewController.h"
//#import "SGShapeView.h"
#import "SGShapeContentViewController.h"
#import <math.h>
#define DEGREES(radians) (radians * 180 / M_PI)

@interface SGViewController ()
- (void)exportEPS;
- (NSArray *)processPoints:(NSArray *)points andString:(NSMutableArray *)pointsString;
@end

@implementation SGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIColor *greyColor = [UIColor colorWithRed:(184.0/255.0) green:(193.0/255.0) blue:(202.0/255.0) alpha:1.0];
    [self.view setBackgroundColor:greyColor];
    
    /* Process EPS File */
    NSString *shapeFilePath = [[NSBundle mainBundle] pathForResource:@"plane" ofType:@"eps"];
    NSError *shapeFileReadError;
    NSString *shapeFileContents = [NSString stringWithContentsOfFile:shapeFilePath encoding:NSUTF8StringEncoding error:&shapeFileReadError];
    NSString *exportShapeFile = [shapeFileContents copy];
    /* shape markers
     newpath
     30.739876 286.866133 moveto
     30.739876 289.133850 lineto
     30.235734 288.629565 lineto
     stroke
     */
    
    NSString *newPath = @"0.000000 0.000000 1.000000 setrgbcolor\nnewpath";
//    newpath";
    
    NSString *stroke = @"stroke";
    
    
    NSRange searchRange = NSMakeRange(0,shapeFileContents.length);
    NSRange shapeStartRange;
    
    self.shapes = [NSMutableDictionary dictionary];
///TODO: remove the first occurance of "newpath" programatically
    int i = 0;
    while (searchRange.location < shapeFileContents.length) {
        searchRange.length = shapeFileContents.length-searchRange.location;
        shapeStartRange = [shapeFileContents rangeOfString:newPath options:NSCaseInsensitiveSearch range:searchRange];
        if (shapeStartRange.location != NSNotFound) {
            searchRange.location = shapeStartRange.location + shapeStartRange.length;
            searchRange.length = shapeFileContents.length-searchRange.location;
            // found an occurrence of the substring! do stuff here
            // add shape to array
            NSRange shapeEndRange = [shapeFileContents rangeOfString:stroke options:NSCaseInsensitiveSearch range:searchRange];
            
            NSInteger shapeEndLength = shapeEndRange.location - (shapeStartRange.location + shapeStartRange.length);
            NSString *rawShapeData = [shapeFileContents substringWithRange:NSMakeRange((shapeStartRange.location + shapeStartRange.length), shapeEndLength)];
            
            // clean up
            NSString *shapeStringToReplace = [rawShapeData copy];
            NSString *shapeString = [rawShapeData copy];
            
            rawShapeData = [rawShapeData stringByReplacingOccurrencesOfString:@"newpath\n" withString:@""];
            rawShapeData = [rawShapeData stringByReplacingOccurrencesOfString:@" moveto" withString:@" lineto"];
            rawShapeData = [rawShapeData stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            NSArray *points = [rawShapeData componentsSeparatedByString:@" lineto"];
            
            i++;
            NSString *shapeName = [NSString stringWithFormat:@"Shape %d", i];
//            NSLog(@"%@", shapeName);
            
            NSMutableArray *singleShape = [NSMutableArray array];
            NSMutableArray *singleShapeString = [NSMutableArray array];
            for (NSString *point in points)
            {
                NSArray *xAndy = [point componentsSeparatedByString:@" "];
                if ([xAndy count] >1)
                {
                    [singleShapeString addObject:point];
                    NSNumber *x = [NSNumber numberWithFloat:[xAndy[0] floatValue]];
                    NSNumber *y = [NSNumber numberWithFloat:[xAndy[1] floatValue]];
                    [singleShape addObject:@[x,y]];
                }
            }
            
            
            
            // Process the shape to remove the notch
            NSArray *shapeWNotch = [self processPoints:singleShape andString:singleShapeString];
            [self.shapes setObject:shapeWNotch[0] forKey:shapeName];
            searchRange.location = shapeEndRange.location + shapeEndRange.length;
            
            
            if ([shapeWNotch[1] count]>1)
            {
                NSArray *stringsToRemove = shapeWNotch[1];
                NSArray *stringsToAdd = shapeWNotch[2];
                
//                NSLog(@"%@",stringsToRemove);
                
                shapeString = [shapeString stringByReplacingOccurrencesOfString:stringsToRemove[1] withString:@""];
                shapeString = [shapeString stringByReplacingOccurrencesOfString:stringsToRemove[2] withString:@""];
                //NSLog(@"the code reaches here");
                NSString *notchString = @"";
                for (NSString *notch in stringsToAdd)
                {
                    notchString = [NSString stringWithFormat:@"%@%@",notchString,notch];
                }
                shapeString = [shapeString stringByReplacingOccurrencesOfString:stringsToRemove[0] withString:notchString];
                //NSLog(@"%@",shapeString);
                exportShapeFile = [exportShapeFile stringByReplacingOccurrencesOfString:shapeStringToReplace withString:shapeString];
            }
            
        } else {
            // no more substring to find
            break;
        }
    }
    
    NSLog(@"%@",exportShapeFile);

    /* Load all shape files */
    _pageTitles = [[self.shapes allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    /* Page view controller */
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    SGShapeContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];

    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);

    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (SGShapeContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count]))
        return nil;
    
    // Create a new view controller and pass suitable data.
    SGShapeContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SGShapeContentViewController"];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    pageContentViewController.points = self.shapes[self.pageTitles[index]];
    
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((SGShapeContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((SGShapeContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (NSArray *)processPoints:(NSArray *)points andString:(NSMutableArray *)pointsString
{
    /*
     triangle
     <------p12------>
     +1-------------+2
     |         a   -
     |         -
     |     -
     | -
     +3
     
     */
    // Detect notch
    int numberOfPoints = [points count];
    for (int i = 0; i < numberOfPoints; i++)
    {
        float x1,y1,x2,y2,x3,y3;
        int point2Index = i+1;
        int point3Index = i+2;
        
        x1 = [points[i][0] floatValue];
        y1 = [points[i][1] floatValue];
        
        // Distance test
        ///TODO: should come from config
        if (x1 > 1)
        continue;
        
        if (i == numberOfPoints - 2)
        {
            point2Index = i+1;
            point3Index = 0;
            
        }else if (i == numberOfPoints - 1)
        {
            point2Index = 0;
            point3Index = 1;
        }
        
        x2 = [points[point2Index][0] floatValue];
        y2 = [points[point2Index][1] floatValue];
        
        x3 = [points[point3Index][0] floatValue];
        y3 = [points[point3Index][1] floatValue];
        
        
        float p12sq = powf(x1-x2, 2) + powf(y1-y2, 2);
        float p23sq = powf(x3-x2, 2) + powf(y3-y2, 2);
        float p13sq = powf(x1-x3, 2) + powf(y1-y3, 2);
        
        // length test
        if ((p12sq < 9) || (p23sq < 9) || (p13sq < 1))
        continue;
        
        /*
         angle 123 = cons-1(p12sq+p)
         */
        
        float angle123 = DEGREES(acosf((p12sq+p23sq-p13sq)/(2*sqrtf(p12sq*p23sq))));
        
        // angle test
        if (angle123 > 15)
        continue;
        
        //        NSLog(@"Notch detected");
        //        NSLog(@"P1: %f, %f", x1, y1);
        //        NSLog(@"P2: %f, %f", x2, y2);
        //        NSLog(@"P3: %f, %f", x3, y3);
        //        NSLog(@"p12sq %f; p23sq %f",p12sq, p23sq);
        
        /* remove old notch */
        NSMutableArray *modifiedShape = [points mutableCopy];
        NSArray *p1 = modifiedShape[i];
        NSArray *p2 = modifiedShape[point2Index];
        NSArray *p3 = modifiedShape[point3Index];
        
        [modifiedShape removeObject:p1];
        [modifiedShape removeObject:p2];
        [modifiedShape removeObject:p3];
        
        NSString *p1Str = [NSString stringWithFormat:@"%@ lineto\n",pointsString[i]];
        NSString *p2Str = [NSString stringWithFormat:@"%@ lineto\n",pointsString[point2Index]];
        NSString *p3Str = [NSString stringWithFormat:@"%@ lineto\n",pointsString[point3Index]];
        NSArray *stringsToRemove = @[p1Str,p2Str,p3Str];
        
        [pointsString removeObject:p1Str];
        [pointsString removeObject:p2Str];
        [pointsString removeObject:p3Str];
        
        /* read new notch */
        NSString *notchFilePath = [[NSBundle mainBundle] pathForResource:@"notch" ofType:@"json"];
        NSError *notchFileReadError;
        NSString *notchFileContents = [NSString stringWithContentsOfFile:notchFilePath encoding:NSUTF8StringEncoding error:&notchFileReadError];
        
        NSData *notchJsonData = [notchFileContents dataUsingEncoding:NSUTF8StringEncoding];
        NSError *notchJsonReadingError;
        NSMutableArray *notch = [NSJSONSerialization JSONObjectWithData:notchJsonData options:NSJSONReadingMutableContainers error:&notchJsonReadingError];
        
        NSMutableArray *stringsToAdd = [NSMutableArray array];
        /* move the new notch */
        for (int j = [notch count]-1; j > -1; j--)
        {
            NSNumber *notchX = [NSNumber numberWithFloat:[notch[j][0] floatValue]+x1];
            NSNumber *notchY = [NSNumber numberWithFloat:[notch[j][1] floatValue]+y1];
            
            [modifiedShape insertObject:@[notchX, notchY] atIndex:i];
            
//            0.000000 202.293558 lineto

            NSString *notchStr = [NSString stringWithFormat:@"%0.6f %0.6f lineto\n", [notch[j][0] floatValue]+x1, [notch[j][1] floatValue]+y1];
            
            [stringsToAdd insertObject:notchStr atIndex:0];
        }
        
        return @[modifiedShape, stringsToRemove, stringsToAdd];
        
    }
    
    return @[points, @[], @[]];
}

- (void)exportEPS
{
//    NSLog(@"exportEPS");
}

@end
