# List of Vertices, with (x,y,z[,w]) coordinates,
v -0.5737027 -0.6162704 0.009842639
v -0.533961 -0.6523914 0.009842639
# Normals in (x,y,z) form; normals might not be unit.
vn 0 0 1
vn 0 0 1
# Texture coordinates, in (u ,v [,w]) coordinates, these will vary between 0 and 1, w is optional and default to 0.
vt -0.6162704 -0.5737027
vt -0.6523914 -0.533961
# Face Definitions (see below)
f 85/85/85 88/88/88 82/82/82
f 19/19/19 20/20/20 21/21/21


# Parameter space vertices in ( u [,v] [,w] ) form; free form geometry statement ( see below )


s 1
