//
//  SGAppDelegate.h
//  Radial Assembly
//
//  Created by Shubham Goel on 5/28/14.
//  Copyright (c) 2014 Shubham Goel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
