/*
  Button
 
 Turns on and off a light emitting diode(LED) connected to digital  
 pin 13, when pressing a pushbutton attached to pin 2. 
 
 
 The circuit:
 * LED attached from pin 13 to ground 
 * pushbutton attached to pin 2 from +5V
 * 10K resistor attached to pin 2 from ground
 
 * Note: on most Arduinos there is already an LED on the board
 attached to pin 13.
 
 
 created 2005
 by DojoDave <http://www.0j0.org>
 modified 30 Aug 2011
 by Tom Igoe
 
 This example code is in the public domain.
 
 http://www.arduino.cc/en/Tutorial/Button
 */

// constants won't change. They're used here to 
// set pin numbers:
const int playButtonPin = 2;     // the number of the pushbutton pin
const int pauseButtonPin = 3;
const int rewindButtonPin = 4;
const int forwardButtonPin = 5;

const int ledPin =  13;      // the number of the LED pin


// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);      
  // initialize the pushbutton pin as an input:
  pinMode(playButtonPin, INPUT);     
  pinMode(pauseButtonPin, INPUT);     
  pinMode(rewindButtonPin, INPUT);     
  pinMode(forwardButtonPin, INPUT);     
  Serial.begin(9600);      // open the serial port at 9600 bps:    

}

void loop(){
  // read the state of the pushbutton value:
  int playButtonState = digitalRead(playButtonPin);
  int pauseButtonState = digitalRead(pauseButtonPin);
  int rewindButtonState = digitalRead(rewindButtonPin);
  int forwardButtonState = digitalRead(forwardButtonPin);

  // check if the pushbutton is pressed.
  // if it is, the buttonState is HIGH:
  if (playButtonState == HIGH) {     
    // turn LED on: 
    Serial.print("Play");
    digitalWrite(ledPin, HIGH);  
  } 
  else {
    // turn LED off:
    digitalWrite(ledPin, LOW); 
  }
  
//  if (pauseButtonState == HIGH) {     
//    // turn LED on: 
//    Serial.print("Pause");
//    digitalWrite(ledPin, HIGH);  
//  } 
//  else {
//    // turn LED off:
//    digitalWrite(ledPin, LOW); 
//  }
//  if (rewindButtonState == HIGH) {     
//    // turn LED on: 
//    Serial.print("Rewind");
//    digitalWrite(ledPin, HIGH);  
//  } 
//  else {
//    // turn LED off:
//    digitalWrite(ledPin, LOW); 
//  }
//  if (forwardButtonState == HIGH) {     
//    // turn LED on: 
//    Serial.print("Froward");
//    digitalWrite(ledPin, HIGH);  
//  } 
//  else {
//    // turn LED off:
//    digitalWrite(ledPin, LOW); 
//  }

  
}
