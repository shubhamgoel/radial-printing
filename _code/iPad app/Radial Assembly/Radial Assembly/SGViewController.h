//
//  SGViewController.h
//  Radial Assembly
//
//  Created by Shubham Goel on 5/28/14.
//  Copyright (c) 2014 Shubham Goel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSMutableDictionary *shapes;

@end
